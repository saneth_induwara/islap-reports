import {BrowserModule} from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';
import {ExceptionalReportComponent} from './report/exceptional-report/exceptional-report.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {createCustomElement} from '@angular/elements';
import {ProgressReportComponent} from './report/progress-report/progress-report.component';
import {ReportModule} from './report/report.module';
import {CashflowReportComponent} from './report/cashflow-report/cashflow-report.component';
import {EvidenceCheckReportComponent} from './report/evidence-check-report/evidence-check-report.component';
import 'hammerjs';
import {LoanSummaryReportComponent} from './report/loan-summary-report/loan-summary-report.component';
import {SensitivityAnalysisComponent} from './report/sensitivity-analysis/sensitivity-analysis.component';
import {ElementZoneStrategyFactory} from 'elements-zone-strategy';

@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReportModule,
  ],
  providers: [],
  entryComponents: [SensitivityAnalysisComponent]
})
export class AppModule {

  constructor(private injector: Injector) {
  }

  ngDoBootstrap() {
    this.defineCustomElements('islap-progress-report', ProgressReportComponent);
    this.defineCustomElements('islap-exceptional-report', ExceptionalReportComponent);
    this.defineCustomElements('islap-cashflow-report', CashflowReportComponent);
    this.defineCustomElements('islap-evidence-check-report', EvidenceCheckReportComponent);
    this.defineCustomElements('islap-loan-summary-report', LoanSummaryReportComponent);
    this.defineCustomElements('islap-sensitivity-analysis-report', SensitivityAnalysisComponent);
  }

  defineCustomElements(name, component) {
    const strategyFactory = new ElementZoneStrategyFactory(component, this.injector);
    const myElement = createCustomElement(component, {injector: this.injector, strategyFactory});
    customElements.define(name, myElement);
  }

}
