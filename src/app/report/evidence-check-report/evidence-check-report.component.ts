import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ReportService} from '../report.service';
import {BehaviorSubject} from 'rxjs';
import * as XLSX from 'xlsx';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-evidence-check-report',
  templateUrl: './evidence-check-report.component.html',
  styleUrls: ['./evidence-check-report.component.scss'],
})
export class EvidenceCheckReportComponent implements OnInit {

  dataSource = new MatTableDataSource<any>();
  questionnaireDataSource = new MatTableDataSource<any>();
  displayedColumns = ['cif', 'name', 'business_decision'];
  questions = [];
  displayedQuestions = [];

  @Input() title: string;
  @Input() subtitle: string;
  @Input() icon: string;
  @Input() export_fle_name: string;
  @Output() on_complete = new EventEmitter<any>();

  private _api_url = new BehaviorSubject<string>('');
  filter = new FormControl();

  constructor(private reportService: ReportService, private changeDetectorRef: ChangeDetectorRef) {
  }

  @Input() set api_url(value: string) {
    this._api_url.next(value);
  }

  get api_url() {
    return this._api_url.getValue();
  }


  ngOnInit() {
    this.reportService.getReports(this.api_url).subscribe(res => {
      if (res !== null) {
        this.questions = res[0].questionnaireResponse.questions;
        this.questions.forEach(item => {
          this.displayedColumns.push(item.id.toString());
          this.displayedQuestions.push(this.getQuestionById(item.id));
        });
        this.dataSource = new MatTableDataSource(res);
        this.questionnaireDataSource = new MatTableDataSource(this.displayedQuestions);
        this.dataSource.filterPredicate = this.customFilterPredicate();
        this.changeDetectorRef.detectChanges();
        this.on_complete.emit({status: 'SUCCESS'});
      }
    }, error => this.on_complete.emit({status: 'FAILED', description: error}));

    this.filter.valueChanges.subscribe(val => {
      this.dataSource.filter = val.toString().toLowerCase().trim();
      this.changeDetectorRef.detectChanges();
    });
  }

  getQuestionById(id) {
    return this.questions.find(q => q.id === id);
  }

  getQuestionResponseById(responses: any[], questionId) {
    return responses.find(q => q.id === questionId);
  }

  customFilterPredicate() {
    const myFilterPredicate = (data, filter: any): boolean => {

      const searchString = filter;
      return data.customer.cif.toString().toLowerCase().trim().indexOf(searchString) !== -1
        || data.customer.name.toString().toLowerCase().trim().indexOf(searchString) !== -1
        || data.businessDecision.toString().toLowerCase().trim().indexOf(searchString) !== -1;
    };
    return myFilterPredicate;
  }

  exportExcel(): void {
    /* table id is passed over here */
    const element = document.getElementById('report-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element, {display: true});

    const table2 = document.getElementById('questionnaire-table');
    const ws2: XLSX.WorkSheet = XLSX.utils.table_to_sheet(table2);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Report');
    XLSX.utils.book_append_sheet(wb, ws2, 'Questionnaire');

    /* save to file */
    XLSX.writeFile(wb, this.export_fle_name ? this.export_fle_name : 'OEIL Check Report' + '.xlsx');
  }

  showTooltip(tooltip) {
    this.changeDetectorRef.detectChanges();
    tooltip.show();
  }

  hideTooltip(tooltip) {
    this.changeDetectorRef.detectChanges();
    tooltip.hide();
  }


}
