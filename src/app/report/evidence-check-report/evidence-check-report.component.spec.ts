import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EvidenceCheckReportComponent} from './evidence-check-report.component';

describe('EvidenceCheckReportComponent', () => {
  let component: EvidenceCheckReportComponent;
  let fixture: ComponentFixture<EvidenceCheckReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvidenceCheckReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvidenceCheckReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
