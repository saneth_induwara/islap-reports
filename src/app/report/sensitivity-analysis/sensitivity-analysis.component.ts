import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl} from '@angular/forms';
import {ReportService} from '../report.service';
import {BehaviorSubject} from 'rxjs';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-sensitivity-analysis',
  templateUrl: './sensitivity-analysis.component.html',
  styleUrls: ['./sensitivity-analysis.component.scss'],
})
export class SensitivityAnalysisComponent implements OnInit {

  displayedColumns = ['cif', 'name', 'os_lkr', 'impairment', 'impairment_advancement',
    'impairment_differment', 'sensitivity_advancement', 'sensitivity_differment'];
  footerDisplayedColumns = ['cif', 'os_lkr', 'impairment', 'impairment_advancement',
    'impairment_differment', 'sensitivity_advancement', 'sensitivity_differment'];
  dataSource = new MatTableDataSource<any>();
  totalOsLKR = 0;
  totalImpairment = 0;
  totalImpairmentAdvancement = 0;
  totalImpairmentDifferment = 0;
  totalSensitivityAdvancement = 0;
  totalSensitivityDifferment = 0;

  filter = new FormControl();
  cycleSelect = new FormControl();


  @Input() title: string;
  @Input() subtitle: string;
  @Input() icon: string;
  @Input() export_fle_name: string;
  @Output() on_complete = new EventEmitter<any>();

  constructor(private reportService: ReportService, private changeDetectorRef: ChangeDetectorRef) {
  }

  private _api_url = new BehaviorSubject<string>('');

  get api_url() {
    return this._api_url.getValue();
  }

  @Input() set api_url(value: string) {
    this._api_url.next(value);
  }

  ngOnInit() {
    this.reportService.getReports(this.api_url).subscribe(res => {
      if (res !== null) {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.filterPredicate = this.customFilterPredicate();
        this.calculateTotals();
        this.changeDetectorRef.detectChanges();
        // setTimeout(() => {this.dataSource.paginator = this.paginator;
        // });
        this.on_complete.emit({status: 'SUCCESS'});
      }
    }, error => this.on_complete.emit({status: 'FAILED', description: error}));

    this.filter.valueChanges.subscribe(val => {
      this.dataSource.filter = val.toString().trim();
      this.changeDetectorRef.detectChanges();
      this.changeDetectorRef.detectChanges();
    });
    this.cycleSelect.setValue(0);
    this.changeDetectorRef.detectChanges();
    this.cycleSelect.valueChanges.subscribe(val => {
      this.changeDetectorRef.detectChanges();
    });

  }

  calculateTotals() {

    let total = [0, 0, 0, 0, 0, 0];

    this.dataSource.filteredData.forEach(item => {
      total[0] += item.osLKR;
      total[1] += item.impairment;
      total[2] += item.impairmentAdvancement;
      total[3] += item.impairmentDifferment;
      total[4] += item.sensitivityAdvancement;
      total[5] += item.sensitivityDifferment;
    });

    this.totalOsLKR = total[0];
    this.totalImpairment = total[1];
    this.totalImpairmentAdvancement = total[2];
    this.totalImpairmentDifferment = total[3];
    this.totalSensitivityAdvancement = total[4];
    this.totalSensitivityDifferment = total[5];

  }

  customFilterPredicate() {
    const myFilterPredicate = (data, filter: any): boolean => {

      const searchString = filter;
      return data.cif.toString().toLowerCase().trim().indexOf(searchString) !== -1
        || data.customerName.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1;
    };
    return myFilterPredicate;
  }

  exportExcel(): void {
    /* table id is passed over here */
    // this.paginator._changePageSize(this.dataSource.data.length);
    const element = document.getElementById('sensitivity-analysis-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element, {display: true});

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.export_fle_name ? this.export_fle_name : 'Sensitivity Analysis Report' + '.xlsx');
    // this.paginator._changePageSize(15);
  }

  onSelectClick() {
    this.changeDetectorRef.detectChanges();
  }
}
