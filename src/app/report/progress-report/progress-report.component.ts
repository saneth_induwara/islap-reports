import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import * as XLSX from 'xlsx';
import {MatTableDataSource} from '@angular/material/table';
import {ReportService} from '../report.service';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-progress-report',
  templateUrl: './progress-report.component.html',
  styleUrls: ['./progress-report.component.scss'],
})
export class ProgressReportComponent implements OnInit {

  dataSource = new MatTableDataSource<any>();
  displayedColumns = ['cif', 'name', 'status', 'segment', 'arm', 'business_approver', 'risk_reviewer'];

  @Input() title: string;
  @Input() subtitle: string;
  @Input() icon: string;
  @Input() export_fle_name: string;
  @Output() on_complete = new EventEmitter<any>();
  filter = new FormControl();

  constructor(private reportService: ReportService, private changeDetectorRef: ChangeDetectorRef) {
  }

  private _api_url = new BehaviorSubject<string>('');

  get api_url() {
    return this._api_url.getValue();
  }

  @Input() set api_url(value: string) {
    this._api_url.next(value);
  }

  ngOnInit() {
    this.reportService.getReports(this.api_url).subscribe(res => {
      if (res !== null) {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.filterPredicate = this.customFilterPredicate();
        this.changeDetectorRef.detectChanges();
        this.on_complete.emit({status: 'SUCCESS'});
      }
    }, error => this.on_complete.emit({status: 'FAILED', description: error}));

    this.filter.valueChanges.subscribe(val => {
      this.dataSource.filter = val.toString().trim();
      this.changeDetectorRef.detectChanges();
    });
  }

  customFilterPredicate() {
    const myFilterPredicate = (data, filter: any): boolean => {

      const searchString = filter;
      return data.arm.toString().toLowerCase().trim().indexOf(searchString.toString().toLowerCase()) !== -1
        || data.businessApprover.toString().toLowerCase().trim().indexOf(searchString.toString().toLowerCase()) !== -1
        || data.riskReviewer.toString().toLowerCase().trim().indexOf(searchString.toString().toLowerCase()) !== -1
        || data.status.toString().toLowerCase().indexOf(searchString.toLowerCase()) !== -1
        || data.customer.cif.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1
        || data.customer.name.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1;
    };
    return myFilterPredicate;
  }

  exportExcel(): void {
    /* table id is passed over here */
    const element = document.getElementById('report-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element, {display: true});

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.export_fle_name ? this.export_fle_name : 'Progress Report' + '.xlsx');
  }
}
