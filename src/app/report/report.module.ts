import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProgressReportComponent} from './progress-report/progress-report.component';
import {ExceptionalReportComponent} from './exceptional-report/exceptional-report.component';
import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {HttpClientModule} from '@angular/common/http';
import {CashflowReportComponent} from './cashflow-report/cashflow-report.component';
import {EvidenceCheckReportComponent} from './evidence-check-report/evidence-check-report.component';
import {LoanSummaryReportComponent} from './loan-summary-report/loan-summary-report.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatPaginatorModule} from '@angular/material/paginator';
import {SensitivityAnalysisComponent} from './sensitivity-analysis/sensitivity-analysis.component';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    ProgressReportComponent,
    ExceptionalReportComponent,
    CashflowReportComponent,
    EvidenceCheckReportComponent,
    LoanSummaryReportComponent,
    SensitivityAnalysisComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatTooltipModule,
    HttpClientModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    ScrollingModule,
    MatPaginatorModule,
    MatSelectModule
  ],
  entryComponents: [
    ProgressReportComponent,
    ExceptionalReportComponent,
    CashflowReportComponent,
    EvidenceCheckReportComponent,
    LoanSummaryReportComponent
  ],
})
export class ReportModule { }
