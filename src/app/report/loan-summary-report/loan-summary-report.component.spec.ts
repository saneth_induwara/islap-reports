import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanSummaryReportComponent } from './loan-summary-report.component';

describe('LoanSummaryReportComponent', () => {
  let component: LoanSummaryReportComponent;
  let fixture: ComponentFixture<LoanSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
