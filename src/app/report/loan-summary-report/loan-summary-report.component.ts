import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ReportService} from '../report.service';
import {BehaviorSubject} from 'rxjs';
import * as XLSX from 'xlsx';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-loan-summary-report',
  templateUrl: './loan-summary-report.component.html',
  styleUrls: ['./loan-summary-report.component.scss'],
})
export class LoanSummaryReportComponent implements OnInit {

  dataSource = new MatTableDataSource<any>();
  displayedColumns = ['cif', 'name', 'prev_assessment_os', 'prev_assessment_provision', 'assessment_os', 'assessment_provision',
    'pl_charge', 'projection_method', 'value', 'date'];

  @Input() title: string;
  @Input() subtitle: string;
  @Input() icon: string;
  @Input() export_fle_name: string;
  @Output() on_complete = new EventEmitter<any>();

  private _api_url = new BehaviorSubject<string>('');
  filter = new FormControl();

  constructor(private reportService: ReportService, private changeDetectorRef: ChangeDetectorRef) {
  }

  @Input() set api_url(value: string) {
    this._api_url.next(value);
  }

  get api_url() {
    return this._api_url.getValue();
  }

  ngOnInit() {
    this.reportService.getReports(this.api_url).subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.getAssessmentOSTotal();
      this.dataSource.filterPredicate = this.customFilterPredicate();
      this.changeDetectorRef.detectChanges();
      this.on_complete.emit({status: 'SUCCESS'});

    }, error => this.on_complete.emit({status: 'FAILED', description: error}));

    this.filter.valueChanges.subscribe(val => {
      this.dataSource.filter = val.toString().trim();
      this.getAssessmentOSTotal();
      this.changeDetectorRef.detectChanges();
    });
  }

  getPreviousAssessmentOSTotal() {
    return this.dataSource.filteredData.map(t => t.previousAssessment !== null ? t.previousAssessment.os : 0)
      .reduce((acc, value) => acc + value, 0);
  }

  getPreviousAssessmentProvisionTotal() {
    return this.dataSource.filteredData.map(t => t.previousAssessment !== null ? t.previousAssessment.provision : 0)
      .reduce((acc, value) => acc + value, 0);
  }

  getAssessmentOSTotal() {
    return (this.dataSource.filteredData.map(t => t.assessment.os).reduce((acc, value) => acc + value, 0));
  }

  getAssessmentProvisionTotal() {
    return this.dataSource.filteredData.map(t => t.assessment.provision).reduce((acc, value) => acc + value, 0);
  }

  getPLChargeTotal() {
    return this.dataSource.filteredData.map(t => this.calculatePLCharge(t.assessment, t.previousAssessment))
      .reduce((acc, value) => acc + value, 0);
  }

  calculatePLCharge(currentAssessment, previousAssessment) {
    return currentAssessment.provision - (previousAssessment !== null ? previousAssessment.provision : 0);
  }

  customFilterPredicate() {
    const myFilterPredicate = (data, filter: any): boolean => {

      const searchString = filter;
      return data.customer.cif.toString().toLowerCase().trim().indexOf(searchString) !== -1
        || data.customer.name.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1;
    };
    return myFilterPredicate;
  }

  exportExcel(): void {
    /* table id is passed over here */
    const element = document.getElementById('report-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element, {display: true});

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.export_fle_name ? this.export_fle_name : 'Loan Summary Report' + '.xlsx');
  }

  showTooltip(tooltip) {
    this.changeDetectorRef.detectChanges();
    tooltip.show();
  }

  hideTooltip(tooltip) {
    this.changeDetectorRef.detectChanges();
    tooltip.hide();
  }

}
