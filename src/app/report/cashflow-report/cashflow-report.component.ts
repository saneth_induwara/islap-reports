import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as XLSX from 'xlsx';
import {MatTableDataSource} from '@angular/material/table';
import {ReportService} from '../report.service';
import {BehaviorSubject} from 'rxjs';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-cashflow-report',
  templateUrl: './cashflow-report.component.html',
  styleUrls: ['./cashflow-report.component.scss']
})
export class CashflowReportComponent implements OnInit {

  headerColumns = ['cif_header', 'name_header', 'accno_header', 'scheme_header', 'intrate_header', 'currency_header',
    'cf_methodology_header', 'as_at', 'projection'];
  displayedColumns = ['cif', 'name', 'accno', 'scheme', 'intrate', 'currency', 'cf_methodology', 'os_original_curr',
    'provision_original_curr', 'os_lkr', 'provision_lkr'];
  dataSource = new MatTableDataSource<any>();
  projectionColumns = [];
  months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER',
    'NOVEMBER', 'DECEMBER'];
  filter = new FormControl();
  // @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;


  @Input() title: string;
  @Input() subtitle: string;
  @Input() icon: string;
  @Input() export_fle_name: string;
  @Input() cycle_start_date: string;
  @Output() on_complete = new EventEmitter<any>();

  constructor(private reportService: ReportService, private changeDetectorRef: ChangeDetectorRef) {
  }

  private _api_url = new BehaviorSubject<string>('');

  get api_url() {
    return this._api_url.getValue();
  }

  @Input() set api_url(value: string) {
    this._api_url.next(value);
  }

  ngOnInit() {
    var startDate = this.cycle_start_date ? new Date(this.cycle_start_date) : new Date();
    startDate.setMonth(startDate.getMonth() + 1);
    this.projectionColumns = this.generateColumns(startDate);
    this.addColumns();
    this.reportService.getReports(this.api_url).subscribe(res => {
      if (res !== null) {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.filterPredicate = this.customFilterPredicate();
        this.changeDetectorRef.detectChanges();
        // setTimeout(() => {this.dataSource.paginator = this.paginator;
        // });
        this.on_complete.emit({status: 'SUCCESS'});
      }
    }, error => this.on_complete.emit({status: 'FAILED', description: error}));

    // this.filter.valueChanges.subscribe(val => {
    //   this.dataSource.filter = val.toString().trim();
    //   this.changeDetectorRef.detectChanges();
    //   this.changeDetectorRef.detectChanges();
    // });
  }

  generateColumns(from: Date) {
    const columns = [];
    const noOfMonths = 60;
    const tmp = from;
    for (let i = 0; i <= noOfMonths; i++) {
      columns.push(this.months[tmp.getMonth()] + '_' + tmp.getFullYear());
      tmp.setMonth(from.getMonth() + 1);
    }
    return columns;
  }

  getValueForFacilityCashFlowElement(facilityCashFlowElements, yearMonth) {
    const tmp = [];
    facilityCashFlowElements.forEach(e => {
      tmp.push(e.month + '_' + e.year.toString());
    });
    const index = tmp.indexOf(yearMonth);
    if (index !== -1) {
      return facilityCashFlowElements[index].amount;
    }
    return 0.00;
  }

  addColumns() {
    this.projectionColumns.forEach(element => {
      this.displayedColumns.push(element);
    });
  }

  calculateTotalOSLKR() {
    let total = 0;
    this.dataSource.filteredData.forEach(item => {
      total += (item.facilityCashFlow.osBalance * item.facilityCashFlow.currencyRate);
    });
    return total;
  }

  calculateTotalProvisionLKR() {
    let total = 0;
    this.dataSource.filteredData.forEach(item => {
      total += item.facilityCashFlow.provisionLKR;
    });
    return total;
  }

  customFilterPredicate() {
    const myFilterPredicate = (data, filter: any): boolean => {

      const searchString = filter;
      return data.customer.cif.toString().toLowerCase().trim().indexOf(searchString) !== -1
        || data.customer.name.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1
        || data.facility.accountNo.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1
        || data.facility.branchCode.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1
        || data.facility.armCode.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1
        || data.facility.industry.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1
        || data.cashFlowType.toString().toLowerCase().trim().indexOf(searchString.toLowerCase()) !== -1;
    };
    return myFilterPredicate;
  }

  exportExcel(): void {
    /* table id is passed over here */
    // this.paginator._changePageSize(this.dataSource.data.length);
    const element = document.getElementById('cashflow-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element, {display: true});

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.export_fle_name ? this.export_fle_name : 'Cashflow Report' + '.xlsx');
    // this.paginator._changePageSize(15);
  }

  onPageChange() {
    this.changeDetectorRef.detectChanges();
  }

  onSearch(val) {
    this.dataSource.filter = val.toString().trim();
    this.changeDetectorRef.detectChanges();
  }
}
